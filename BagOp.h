// hi there!
#ifndef _BAG_H
#define _BAG_H

#include <iostream>
using std::ostream;  using std::istream;

// class members private by default

class Bag {
  int sz; // actual number of elements
  int capacity;  // how many it can hold
  int *intbag;

  static int CAPLIMIT;  // shared by all Bag objects

 public:
  Bag();  // default constructor
  Bag(int);   // conversion constructor
  Bag(const Bag &);  // copy constructor - DEEP copy
  ~Bag();   // destructor!!

  // accessors - do not modify object
  // const applies to object calling function
  int size() const { return sz; } ;  // inline definition
  bool isEmpty() const { return sz == 0; } ;
  bool isFull() const { return sz == capacity; } ;
  bool find(int) const;
  int cap() const { return capacity; } ;
  ostream & write(ostream &) const;

  const int & operator[](int) const;   // read only access
  bool operator==(const Bag &) const;
  bool operator!=(const Bag &) const;
  const Bag & operator+(const Bag &) const;

  // modifiers  
  bool add(int);
  bool remove(int);
  istream & read(istream &);

  const Bag & operator=(const Bag &);  // want deep copy
  const Bag & operator+=(const Bag &);
  int & operator[](int);   // lvalue element from bag, read/write access

  static int CapLimit(int);   // change static variable

  // helpers
 private:
  int where(int) const;

} ;  // must have semicolon

#endif
