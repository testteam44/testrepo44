#include "BagOp.h"

using namespace std;

int main(void)
{
    Bag b1(5);
    b1.add(10);
    b1.add(150);
    Bag b2;
    b2 = b1;          // implicity uses overloaded =
    b2.operator=(b1); // "operator=" is just a method, can in fact directly invoke it
    
    Bag & b1alias = b1;
    b1alias = b1;    // self assignment - need to be careful
    
    b2.add(20);
    b2.add(40);
    b2 += b1;
    
    // prevent this:  (b2 += b1) = Bag(10);
    // don't let return of += be an lvalue
    
    int n1, n2, n3;
    n1 = n2 = n3 = 10;
    
    Bag b3(5);
    b3.add(1);
    b3.add(2);
    b3.add(3);
    
    cout << b3[0] << endl;
    
    b3[0] = 1000;
    
    const Bag b4(b2);
    cout << b4[2] << endl; // matches the [] const member function since b4 const
    //  b4[2] = 13;   compiler error - [] const member function returns a copy not a reference
}
