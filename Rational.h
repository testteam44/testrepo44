#ifndef _RATIONAL_H
#define _RATIONAL_H

#include <iostream>
#include <stdexcept>

using std::ostream;

class Rational
{
private:
    int nume;  // numerator
    int denom;  // denominator
    void reduce();
    
public:
    Rational(int n = 0, int d = 1) throw (std::domain_error);
    
    Rational operator-() const;
    
    // cast operator overloads
    operator int() const;
    operator double() const;
    
    double compare(const Rational &) const;
    
    Rational operator+(const Rational &) const;
    
    bool operator==(const Rational &rhs) const;
    
    bool operator<=(const Rational & rhs) const
    { return compare(rhs) <= 0; } ;
    
    bool operator<(const Rational & rhs) const
    { return this->compare(rhs) < 0; } ;
    
    bool operator>(const Rational & rhs) const
    { return this->compare(rhs) > 0; } ;
    
    bool operator>=(const Rational & rhs) const
    { return *this > rhs || *this == rhs; } ;
    
    // mutating operators
    // default operator = is member-wise = => sufficient
    Rational & operator+=(const Rational &);
    
    // overloading of << must be done my non-method independent function
    // because first argument of << is "not us", its an ostream
    friend ostream& operator<<(ostream &, const Rational &);
    
}; // end of Rational

#endif
