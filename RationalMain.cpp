#include "Rational.h"
#include <stdexcept>

using std::cout; using std:: endl;
using std::ostream; using std::cerr;
using std::cin;

int main()
{
  Rational zero;    // 0/1
  Rational two(2);  // 2/1
  Rational third(2, 6);  // 1/3

  Rational r1, r2, r3;
  r1 = two + third;
  cout << "r1: " << r1 << endl;
  two += Rational(13, 245);
  cout << "two: " << two << endl;

  /* Problem: this could match three versions of + because of 
     casting operators being implicitly called

  r2 = third + 5;   // implicitly calls conversion constructor
  */

  r2 = third + Rational(5);  // removes operator ambiguity

  cout << "r2: " << r2 << endl;

  /*  output created (reduce not implemented)

r1: 14/6
two: 503/245
r2: 32/6
r: 5 d: 5.33333

  */


  int r = (int) r2;
  double d = (double) r2;
  cout << "r: " << r << " d: " << d << endl;

  r3 = -r2;

  if (r1 > r2)
    cout << "r1 > r2" << endl;

  if (two <= third)
    cout << "two <= third" << endl;

  Rational good;

  try {
    int nume, denom;
    cout << "enter numerator & denominator: ";
    cin >> nume >> denom;
    r3 += 10;
    Rational bad(nume, denom);  // may throw domain_error
    // rest of try block will not execute if an exception was thrown
    cout << "joanne" << endl;
    if (nume > 100)
      throw 100;     // can throw any data type
    cout << bad << endl;
    good = bad;   // copy values
  }
  catch (const std::domain_error & e) {
    cerr << "fraction initialization error" << endl;
    cerr << e.what() << endl;
  }
  catch (...) {  // catch anything not already caught, of any type!
    cerr << "mystery errors" << endl;
  }

  // program continues here after try and catch blocks executed
  //   cout << bad << endl;  // bad out of scope & destroyed
  cout << "good is " << good << endl;

}
